using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace spotify_overlay_app.SpotifyWrapper
{
    public class SpotifyApiHttpClient
    {
        private readonly IHttpClientFactory _clientFactory;

        public SpotifyApiHttpClient(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task OnGet()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "url");

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                
            }
        }
    }
}